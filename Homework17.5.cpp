#include <iostream>
class Vector
{
private:
    double x;
    double y;
    double z;

public:
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << "Vector values: x = " << x << ", y = " << y << ", z = " << z << std::endl;
    }
    double length()
    {
        return std::sqrt(x * x + y * y + z * z);
    }

};
int main()
{
    Vector v(10, 10, 10);

    v.Show();

    std::cout << "Length vector = " << v.length() << std::endl;

    return 0;
}
